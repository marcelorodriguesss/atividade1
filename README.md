## O arquivo representa o total de precipitação diária em milímetros sobre o estado do Ceará.

## Usando a série diária de precipitação sobre o estado do Ceará, faça o seguinte:

1. Remover da série temporal dias sem informações de precipitação (nan);

2. Calcular acumulado mensal de precipitação para todos os meses da série temporal;

3. Calcular a climatologia do acumulado mensal de precipitação para todos os meses da série temporal. A climatologia é a média do acumulado de chuva para um determinado mês, por exemplo: Para calcular a climatologia do mês de Janeiro, faz-se a média de todos os "Janeiros" da série.

4. Plotar em gráfico de linhas os resultados do item 3, no eixo X os meses da climatologia e no eixo Y os milímetros.

5. Calcular o acumulado anual de precipitação para todos os anos da série temporal;

6. Plotar em gráfico de barras os resultados do intem 5;

7. Calcular o acumulado trimestral para toda a série de precipitação. Por exemplo: Acumulado de Janeiro, Fevereiro e Março de 1981, Acumulado de Fevereiro, Março e Abril de 1981, Acumulado de Março, Abril e Maio de 1981, ..., até Outubro, Novembro e Dezembro 2010;

8. Plotar em gráfico de linhas os resultados do item 7;

9. [DESAFIO] Plotar gráficos em Dashboard ([Streamlit](https://streamlit.io) ou [Plotly Dash](https://dash.plotly.com)).

## Bibliotecas que podem ser úteis:

[Matplotlib](https://matplotlib.org/),
[Pandas](https://pandas.pydata.org/),
[Xarray](https://docs.xarray.dev/en/stable/),
[Streamlit](https://streamlit.io/),
[Plotly](https://dash.plotly.com),
[Numpy](https://numpy.org/),
[Docker](https://www.docker.com/),
[Kubernetes](https://kubernetes.io/pt-br/)
